# r7ar - Alpha 2
Rapid snapshot archiver is a program forked from GNU Tar to change from tape archiver to a snapshot archiver using the incremental backup.

<h2>Prerequisites</h2>

- automake
- autoconf
- base-devel
- bison
- gcc
- git
- m4

The required packages may differ on some Linux distributions. 

<h2>Build</h2>
1. `cd` to the directory containing the package's source code and run `./configure` to configure the package for your system.
2. Run `make` to compile the package, if the build success the binary will be created at src folder
3. Optionally, run `make check` to run a self-tests that come with the package
4. Run `make install` to install the programs, data files, and documentation

Please report an issue if the build fails on your system architectures.