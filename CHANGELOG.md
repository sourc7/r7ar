r7ar - Alpha 2  (26 June, 2015)
    
    * src/multiple: Removed legacy Star archive header
    * configure: Removed configure autoconf version check
    * src/incremental.c: Removed incremental archiving if the ctime is different

r7ar - Alpha 1 (20 June, 2015)

    * src/extract.c: Replace the file if mtime age is different with disabled verbose output

r7ar - initial release (17 June, 2015)
    
    * all: Added GNU Tar files

See TODO for future version information